import structures.Piece;
import structures.Pos;

public class Main {

    
    public ArrayList<Pos> getPossibleMoves(Piece p) {
        ArrayList<Pos> result = new ArrayList<Pos> ();
        if(/*Logic for Queen*/) {
            result.addAll(getQueenMoves(p));
        }
        //and so on
        return result;
    }

    public ArrayList<Pos> getQueenMoves(Piece p) {
        ArrayList<Pos> result = new ArrayList<Pos> ();
        
        //This part should be put in separate direction functions
        int xCoord = p.currentPos.getX();
        int yCoord = p.currentPos.getY();
        
        //For East Direction
        while(isValid(xCoord,yCoord)) {
            Pos posObject = new Pos(xCoord, yCoord);
            result.add(posObject);
            yCoord++;
        }
        
        //and so on for other directions
            
        return result;
    }
        
    
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}