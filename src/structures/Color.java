package structures ; 
public class Color {
	public static final int NOTHING = 0 ; 
	public static final int WHITE = 1 ; 
	public static final int BLACK = 2 ; 
	
	public static int getColor(Piece p) {
		if (p.color == 1) {
			return WHITE ; 
		}
		if (p.color == 2) {
			return BLACK ; 
		}
		return NOTHING ;
		
	}
}
