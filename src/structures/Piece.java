package structures;
import structures.* ; 
public class Piece {
	public int id ; 
	public Pos currentPos ; 
	public int color ; 
	public boolean alive ; 
	
	public Piece(int id, Pos p, int color, boolean alive) {
		this.id = id ; 
		this.color = color ; 
		this.currentPos = p ; 
		this.alive = alive ; 
	}
	public Piece(int id, Pos p, int color) {
		this.id = id ; 
		this.color = color ; 
		this.currentPos = p ; 
		this.alive = true ; 
	}
	public int getId(Piece p) {
		 return Type.getType(p) ; 
	}
	
	public int getColor(Piece p) {
		return Color.getColor(p) ; 
	}
	
}