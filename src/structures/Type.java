package structures; 
import structures.Piece;

public class Type {
	public static final int NOTHING = 0 ; 
	public static final int KING = 1 ; 
	public static final int QUEEN = 2 ; 
	public static final int BISHOP = 3 ; 
	public static final int KNIGHT = 4 ; 
	public static final int ROOK = 5 ; 
	public static final int PAWN = 6 ; 
	
	public static int getType(Piece p) {
		if (p.id == 0) {
			return NOTHING ; 
		}
		if (p.id == 1) {
			return KING ; 
		}
		if (p.id == 2) {
			return QUEEN ; 
		}
		if (p.id == 3) {
			return BISHOP ; 
		}
		if (p.id == 4) {
			return KNIGHT ; 
		}
		if (p.id == 5) {
			return ROOK ; 
		}
		if (p.id == 6) {
			return PAWN ; 
		}
		return NOTHING;
		
	}
	
}
